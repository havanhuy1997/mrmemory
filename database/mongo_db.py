import pymongo
from database.csv_writer import writerCSV
class MongoDatabase:
    instance = None
    def __init__(self, url, name_db, name_collection):
        self.url = url
        self.name_db = name_db
        self.name_collection = name_collection
        self.client = pymongo.MongoClient(self.url)
        self.db = self.client[self.name_db]
        self.collection = self.db[self.name_collection]
    def connect(self, url, name_db, name_collection):
        if self.instance:
            return self.instance
        else:
            self.instance = MongoDatabase(url, name_db, name_collection)
            return self.instance
    def close(self):
        self.client.close()
        self.instance = None
        del self
    def insert_one(self, data):
        x = self.collection.insert_one(data)
        print('INSERTED TO DATABASE : ' + str(x.inserted_id))
    def get_all_row(self):
        return self.collection.find()
    def query_url(self,url,is_ram_url):
        if is_ram_url:
            query = {'ram_url' : url}
        else:
            query = {'hdd_url' : url}
        return self.collection.find(query).count()
    def create_csv(self, out_file):
        csv = writerCSV(out_file)
        csv.open_csv()
        data_dict = self.get_all_row()
        csv.write_all_row(data_dict)
        csv.close()
        print('CREATED THE CSV FROM DATABASE')

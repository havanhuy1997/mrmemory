import csv
import os
class writerCSV:
	def __init__(self, name_file):
		self.name_file =  name_file
	def open_csv(self):
		if os.path.exists(self.name_file):
			self.myfile = open(self.name_file,'a', newline='')
			self.writer = csv.writer(self.myfile, delimiter=',', quotechar='"')
		else:
			self.myfile = open(self.name_file,'w', newline='')
			self.writer = csv.writer(self.myfile, delimiter=',', quotechar='"')
			headerRow = ['brand', 'range','series','model','make_model','ram_url','total_memory_slots','maximum_memory','memory_type','voltage','speed','hdd_url','sata_interface','driver_form_formats']
			self.writer.writerow(headerRow)
			self.myfile.flush()
	
	def write(self, data_dict_cursor):
		data_save = []
		for i in data_dict_cursor.values():
			data_save.append(i)
		self.writer.writerow(data_save[0:-1])
		self.myfile.flush()
	def write_all_row(self, data_dict_cursor):
		for i in range(data_dict_cursor.count()):
			data = []
			a = data_dict_cursor[i]
			for j in a.values():
				data.append(j)
			self.writer.writerow(data[1:])
	def close(self):
		self.myfile.close()
1. This program uses python3

2. Requirements: use below command to install
pip install -r requirements.txt

3. How to use the program:
python main.py --url-file=FILE.XML --database-connection=CONNECTION --output-file=FILE.CSV --dump-db
--url-file : url of sitemap, eccept only one of these url :
            https://www.mrmemory.co.uk/sitemaps/sitemapmodels1.xml
            https://www.mrmemory.co.uk/sitemaps/sitemapmodels2.xml
            https://www.mrmemory.co.uk/sitemaps/sitemapmodels3.xml
            https://www.mrmemory.co.uk/sitemaps/sitemapmodels4.xml
            https://www.mrmemory.co.uk/sitemaps/sitemapmodels5.xml
            https://www.mrmemory.co.uk/sitemaps/sitemapmodels6.xml
            https://www.mrmemory.co.uk/sitemaps/sitemapmodels7.xml
            https://www.mrmemory.co.uk/sitemaps/sitemapmodels8.xml
            https://www.mrmemory.co.uk/sitemaps/sitemapmodels9.xml
            https://www.mrmemory.co.uk/sitemaps/sitemapmodels10.xml
            These links include urls of all computers
            if --ur-file is not provide, the program will scrape entire website
--database-connection : the url to connect to mongodb
            default value : mongodb://localhost:27017/
--output-file : the name of csv
            default value : out.csv  
--dump-db : Only create csv from database

python main.py --help : see all options of the script


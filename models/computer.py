from utils.util import get_soup
import re
class Computer:
    def _find_url(self, url):
        parts = url.split('/')
        if parts[3] == 'memory-ram-upgrades':
            parts[3] = 'ssd-upgrades'
            return url, '/'.join(parts)
        elif parts[3] == 'ssd-upgrades':
            return '', url
        else:
            return '',''
    def _get_spec_from_prod_field(self, prod_field, text):
        if prod_field.get_text() == text:
            a = prod_field.parent.get_text().split('\n') 
            for i in a[::-1]:
                if i:
                    return i
        return ''
    def __init__(self, brand, _range, series, model, ram_url):
        self.brand = brand
        self.range = _range
        self.series = series
        self.model = model
        self.make_model = ''
        self.ram_url, self.hdd_url = self._find_url(ram_url)
        self.total_memory_slots = ''
        self.maximum_memory = ''
        self.memory_type = ''
        self.voltage = ''
        self.speed = ''
        self.sata_interface = ''
        self.driver_form_formats = ''
    def find_info(self, proxy, list_proxy):
        #get info ram
        if self.ram_url:
            soup =  get_soup(self.ram_url, proxy, list_proxy)
            if soup is None:
                print('ERROR TO GET INFO COMPUTER')
                return None
            prod_fields =  soup.select('.ProdField')
            for prod_field in prod_fields:
                if not self.make_model:
                    self.make_model = self._get_spec_from_prod_field(prod_field,'Make & Model:')
                if not self.total_memory_slots:
                    total_memory_slots = self._get_spec_from_prod_field(prod_field, 'Total Memory Slots:')
                    a = re.search("^([0-9\.]*)", total_memory_slots)
                    if total_memory_slots and a:
                        self.total_memory_slots = a.group(1)
                if not self.maximum_memory:
                    maximum_memory = self._get_spec_from_prod_field(prod_field, 'Maximum Memory:')
                    a = re.search("^([0-9\.]*)", maximum_memory)
                    if maximum_memory and a:
                        self.maximum_memory = a.group(1) 
                if not self.memory_type:
                    self.memory_type = self._get_spec_from_prod_field(prod_field, 'Technology:')
                if not self.speed:
                    self.speed = self._get_spec_from_prod_field(prod_field,'Speed:')
                if not self.voltage:
                    self.voltage = self._get_spec_from_prod_field(prod_field, 'Voltage:')
        
        #get info ssd
        if self.hdd_url:
            soup =  get_soup(self.hdd_url, proxy, list_proxy)
            if soup is None and self.memory_type == '' and self.speed == '' and self.voltage == '':
                print('ERROR TO GET INFO SSD')
                return None
            prod_fields = soup.select('.ProdField')
            for prod_field in prod_fields:
                if not self.sata_interface:
                    self.sata_interface = self._get_spec_from_prod_field(prod_field, 'Interface:')
                if not self.driver_form_formats:
                    self.driver_form_formats =  self._get_spec_from_prod_field(prod_field,'Drive Form Factor:')
        if self.ram_url or self.hdd_url:
            data = {
            'brand' : self.brand,
            'range' :self.range,
            'series' : self.series,
            'model' : self.model,
            'make_model' : self.make_model,
            'ram_url' : self.ram_url,
            'total_memory_slots' : self.total_memory_slots,
            'maximum_memory' : self.maximum_memory,
            'memory_type' : self.memory_type,
            'voltage' : self.voltage,
            'speed' : self.speed,
            'hdd_url' : self.hdd_url,
            'sata_interface' : self.sata_interface,
            'driver_form_formats' : self.driver_form_formats
            }
            return data
        return None
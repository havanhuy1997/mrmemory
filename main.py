import click
from database.mongo_db import MongoDatabase
from database.csv_writer import writerCSV
from scrapers.scraper_from_sitemap import scrape_sitemap 
from scrapers.scraper_all import scrape_all

@click.command()
@click.option('--url-file', 'url_file_sitemap',
				help="The url of sitemap "
				"like that : https://www.mrmemory.co.uk/sitemaps/sitemapmodels1.xml")
@click.option('--database-connection', 'database_connection', 
				default='mongodb://localhost:27017/',
				help="The url to connect to mongodb "
				"like : mongodb://localhost:27017/")
@click.option('--output-file', 'output_file',
				default='out.csv',
				help="The name of file csv.")
@click.option('--dump-db', 'dum_db', is_flag=True, 
				help="Create csv from database.")
def main(url_file_sitemap, database_connection, output_file, dum_db):
	db = MongoDatabase.connect(MongoDatabase, database_connection, 'mydatabase', 'computer')
	if dum_db:
		#create csv from databseGETTING URL FROM SITE_MAP
		db.create_csv(output_file)
		db.close()
	else:
		csv = writerCSV(output_file)
		csv.open_csv()
		if url_file_sitemap:
			#scrape from 1 sitemap
			try:
				print('STARTING TO SCRAPE FROM SITE_MAP')
				scrape_sitemap(url_file_sitemap, db, csv)
			except:
				print('TERMINATE SCRAPE_SITEMAP')
			finally:
				csv.close()
				db.close()
		else:
			#scrape all
			print('STARTING TO SCRAPE ALL')
			scrape_all(db, csv)
if __name__ == '__main__':
	main()
import requests
import random

from bs4 import BeautifulSoup
def get_proxies(name_file):
    f = open(name_file, 'r')
    proxies = f.readlines()
    for i in range(len(proxies)):
        proxies[i] = proxies[i].strip()
    f.close()
    return proxies
def get_last_url_scraped(name_file):
    f = open(name_file, 'r')
    list_url = f.readlines()
    f.close()
    for i in range(len(list_url)):
        list_url[i] = list_url[i].strip()
    if len(list_url) == 3:
        last_brand_url = list_url[0]
        last_range_url = list_url[1]
        last_series_url = ''
        last_model_url = list_url[2]
    elif len(list_url) == 4:
        last_brand_url = list_url[0]
        last_range_url = list_url[1]
        last_series_url = list_url[2]
        last_model_url = list_url[3]
    elif len(list_url) == 2:
        last_brand_url = list_url[0]
        last_range_url = list_url[1]
        last_series_url = ''
        last_model_url = ''
    elif len(list_url) == 1:
        last_brand_url = list_url[0]
        last_range_url = ''
        last_series_url = ''
        last_model_url = ''
    else:
        last_brand_url = ''
        last_range_url = ''
        last_series_url = ''
        last_model_url = ''

    return last_brand_url, last_range_url, last_series_url, last_model_url
def get_names_from_url(url):
    brand_name = range_name = series_name = model_name = ''
    parts = url.split('/')
    brand_name = parts[4]
    range_name = parts[5]
    model_name = parts[-1]
    is_ram_url = False
    if len(parts) == 8:
        series_name = parts[-2]
    if parts[3] == 'memory-ram-upgrades':
        is_ram_url = True
    return brand_name, range_name, series_name, model_name, is_ram_url
 
def save_last_url(name_file, data):
    f = open(name_file, 'w')
    for i in data:
        f.write(i + '\n')
    f.close()
def get_soup(url, proxy, list_proxy, rotate=False):
    if rotate:
        proxy = random.choice(list_proxy).strip()
    i = 0
    while i<5:
        proxies = {'http': 'http://%s' % proxy, 'https': 'http://%s' % proxy}
        try:
            r = requests.get(url, proxies=proxies, timeout=10)
            soup = BeautifulSoup(r.content, features="lxml")
            return soup
        except:
            if proxy in proxies:
                del list_proxy[list_proxy.index(proxy)]
            print('ROTATING PROXY...')
            proxy = random.choice(list_proxy).strip
            i += 1
    print('ERROR PROXY LIST')
    exit()
    return None   
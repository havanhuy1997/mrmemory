import requests
import random
from bs4 import BeautifulSoup
from utils.util import get_proxies, get_soup, get_last_url_scraped, save_last_url
from models import computer
from database.mongo_db import MongoDatabase
from database.csv_writer import writerCSV
from utils.util import get_names_from_url

def scrape_all(db, csv):
    sitemap = 'https://www.mrmemory.co.uk/sitemap'
    proxy = '120.138.21.24:3128'
    list_proxy = get_proxies('./resource/list_proxy.txt')
    last_brand_url, last_range_url, last_series_url, last_model_url = get_last_url_scraped('./resource/last_url_scraped.txt')
    soup = get_soup(sitemap, proxy, list_proxy)
    brands = soup.select('.contentFullwidth_2col')[1].select('.master')
    brand_name = range_name = series_name = model_name = ''

    #configure last_urls
    brand_find = range_find = series_find = model_find = False
    try:
        for a in brands:
            #get info brand
            brand_url = a.attrs['href']
            if last_brand_url:
                if brand_url != last_brand_url and not brand_find:
                    continue
            brand_find = True
            brand_name = a.get_text().strip()

            soup = get_soup(brand_url, proxy, list_proxy)

            ranges = soup.select('#filterContent')[0].select('.master')
            for b in ranges:
                #get info range
                range_url = b.attrs['href']
                if last_range_url:
                    if range_url != last_range_url and not range_find:
                        continue
                range_find = True
                range_name = b.get_text().strip()

                soup = get_soup(range_url, proxy, list_proxy)
                models = soup.select('#filterContent')[0].select('.master')
                soup = get_soup(models[0].attrs['href'], proxy, list_proxy)
                if soup.select('#filterContent'):
                    for seri in models:
                        #get info series
                        series_url = seri.attrs['href']
                        if last_series_url:
                            if series_url != last_series_url and not series_find:
                                continue
                        series_find = True
                        series_name = seri.get_text().strip()

                        soup = get_soup(series_url, proxy, list_proxy)
                        models = soup.select('#filterContent')[0].select('.master')
                        for a in models:
                            #get info module
                            model_url = a.attrs['href']
                            if last_model_url:
                                if model_url != last_model_url and not model_find:
                                    continue
                            model_find = True
                            model_name = a.get_text().strip()

                            #save last urls +find info + write to database + csv
                            last_brand_url = brand_url
                            last_range_url = range_url
                            last_series_url = series_url
                            last_model_url = model_url

                            _, _, _, _, is_ram_url = get_names_from_url(model_url)
                            if not db.query_url(model_url, is_ram_url):
                                try:
                                    print('SCRAPING : ' + model_url)
                                    c = computer.Computer(brand_name, range_name, series_name, model_name, model_url)
                                    data_dict = c.find_info(proxy, list_proxy)
                                    print(data_dict)
                                    if data_dict:
                                        db.insert_one(data_dict)
                                        csv.write(data_dict)
                                except KeyboardInterrupt:
                                    print('TERMINATE SCRAPE_ALL')
                                    csv.close()
                                    db.close()
                                    save_last_url('./resource/last_url_scraped.txt', [last_brand_url, last_range_url, last_series_url, last_model_url])
                                except:
                                    print('IGNORE : ' + model_url)
                                
                else:
                    for model in models:
                        #get info module
                        model_url = model.attrs['href']
                        if last_model_url:
                            if model_url != last_model_url and not model_find:
                                continue
                        model_find = True
                        model_name = model.get_text().strip()

                        #save last urls find info + write to database + csv
                        last_brand_url = brand_url
                        last_range_url = range_url
                        last_series_url = ''
                        last_model_url = model_url

                        _, _, _, _, is_ram_url = get_names_from_url(model_url)
                        if not db.query_url(model_url, is_ram_url):
                            try:
                                print('SCRAPING : ' + model_url)
                                c = computer.Computer(brand_name, range_name, series_name, model_name, model_url)
                                data_dict = c.find_info(proxy, list_proxy)
                                print(data_dict)
                                if data_dict:
                                    db.insert_one(data_dict)
                                    csv.write(data_dict)
                            except:
                                print('IGNORE : ' + model_url)
                                
    except:
        print('TERMINATE SCRAPE_ALL')
    finally:
        csv.close()
        db.close()
        save_last_url('./resource/last_url_scraped.txt', [last_brand_url, last_range_url, last_series_url, last_model_url])
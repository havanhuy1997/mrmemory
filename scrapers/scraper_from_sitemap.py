import requests
import random
from bs4 import BeautifulSoup
from utils.util import get_proxies, get_soup, get_last_url_scraped, save_last_url, get_names_from_url
from models import computer

def scrape_sitemap(url_site_map, db, csv):
    proxy = '120.138.21.24:3128'
    list_proxy = get_proxies('./resource/list_proxy.txt')
    print('GETTING URL FROM SITE_MAP')
    soup = get_soup(url_site_map, proxy, list_proxy)
    url_tags = soup.select('loc')
    for url_tag in url_tags:
        url = url_tag.get_text()
        brand_name, range_name, series_name, model_name, is_ram_url = get_names_from_url(url)
        if not db.query_url(url, is_ram_url):
            print('SCRAPING : ' + url)
            try:
                c = computer.Computer(brand_name, range_name, series_name, model_name, url)
                data_dict = c.find_info(proxy, list_proxy)
                print(data_dict)
                if data_dict:
                    db.insert_one(data_dict)
                    csv.write(data_dict)
            except:
                print('IGNORE : ' + url)